var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync");
var babel = require('gulp-babel');
var zip = require("gulp-zip");
var del = require("del");
var merge = require("merge-stream");
var moment = require("moment");

gulp.task("browser-sync", function() {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
});

gulp.task("sass", function() {
	return gulp.src( "app/scss/*.scss" )
		.pipe(sass())
		.pipe(gulp.dest( "app/css" ))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task("watch", [ "browser-sync", "sass", "scripts" ], function() { 
	gulp.watch( "app/scss/*.scss", ["sass"] ).on( "change", browserSync.reload );
	gulp.watch( "*.html" ).on( "change", browserSync.reload );
	gulp.watch( "app/js/*.js" ).on( "change", browserSync.reload );
});

gulp.task('scripts', function() {
    return gulp.src( 'app/js/lib/*.js' )
		.pipe(babel({
			presets: ['@babel/preset-env']
		}))
		.pipe(gulp.dest('app/js'))
});

gulp.task("default", ["watch"]);



gulp.task("empty", function() {
	return del.sync(["dist/**/*"]);
});

gulp.task("build-move", ["empty", "sass"], function() {
	var app = gulp.src("app/css/*").pipe(gulp.dest("dist/app/css/"));
	var data = gulp.src("app/data/*").pipe(gulp.dest("dist/app/data/"));
	var media = gulp.src("app/images/*").pipe(gulp.dest("dist/app/images/"));
	var js = gulp.src(["app/js/*", "!app/js/lib/*", "!app/js/lib"]).pipe(gulp.dest("dist/app/js/"));
	var vendor = gulp.src("vendor/*").pipe(gulp.dest("dist/vendor"));
	var fonts = gulp.src("vendor/fonts/*").pipe(gulp.dest("dist/vendor/fonts/"));
	var index = gulp
		.src(["index.html"])
		.pipe(gulp.dest("dist/"));

	return merge(app, data, media, js, vendor, fonts, index);
});

gulp.task("build", ["build-move"], function() {
	var currentDate = moment().format("DDMMYY");
	var packageJSON = require("./package.json");
	var packageName = packageJSON.name;
	var packageVer = packageJSON.version.substring(
		0,
		packageJSON.version.length - 2
	);

	return gulp
		.src("dist/**/*")
		.pipe(
			zip(
				`${packageName}_${currentDate}_v${packageVer}.zip`
			)
		)
		.pipe(gulp.dest("./"));
});
