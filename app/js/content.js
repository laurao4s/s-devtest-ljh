"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

 =====================================================
 SPONGE UK DEVELOPER TEST
 JSON parser and event handler
 =====================================================
*/
var objContent = {};

var Content =
/*#__PURE__*/
function () {
  function Content(strDataLocation) {
    _classCallCheck(this, Content);

    this.strDataLocation = strDataLocation;
  }

  _createClass(Content, [{
    key: "init",
    value: function init(_func) {
      $.getJSON(this.strDataLocation, function (objResponse) {
        objContent = objResponse; //callback once json loaded

        _func();
      });
    }
  }, {
    key: "getItem",

    /**
     * Get an item from the content data
     */
    value: function getItem(intItem) {
      return objContent[intItem];
    }
  }, {
    key: "populateContent",

    /**
     * Populate the content
     */
    value: function populateContent($source, _item, $elem) {
      try {
        var source = document.getElementById($source);
        var strHeaderSource = source.innerHTML,
            resHeaderTemplate = Handlebars.compile(strHeaderSource),
            strHeaderHTML = resHeaderTemplate(this.getItem(_item));
        var elem = document.getElementById($elem);
        elem.insertAdjacentHTML('beforeend', strHeaderHTML);
      } catch (error) {
        console.error("The following error has occured populating the content (" + $source + "):\n\n" + error);
      }
    }
  }]);

  return Content;
}();

var Accordion =
/*#__PURE__*/
function () {
  function Accordion() {
    _classCallCheck(this, Accordion);
  }
  /**
   * Populate the accordion within the element ($elem) param
   */


  _createClass(Accordion, [{
    key: "populateAccordion",
    value: function populateAccordion($elem) {
      try {
        (function () {
          var el = document.getElementById($elem);
          var items = el.querySelectorAll(".accordion-item");
          var contents = el.querySelectorAll(".accordion-item-content"); //loop through each accordion item

          var _loop = function _loop(i) {
            //set first accordion item as active
            if (i == 0) ActiveAccordionItem(items[i], contents[i], true);else //and hide the rest of the items content
              ActiveAccordionItem(items[i], contents[i], false); //set click interactivity

            items[i].addEventListener("click", function () {
              //reset all accordion items and hide all content
              for (var j = 0; j < items.length; j++) {
                ActiveAccordionItem(items[j], contents[j], false);
              } //set selected as active and display the corresponding content


              ActiveAccordionItem(items[i], contents[i], true);
            });
          };

          for (var i = 0; i < items.length; i++) {
            _loop(i);
          }
        })();
      } catch (error) {
        console.error("The accordion (" + $elem + ") cannot be found. \n\n" + error);
      }
    }
  }]);

  return Accordion;
}(); //set the active state of an accordion item and it's corrsponding content


function ActiveAccordionItem($elem, $content, _active) {
  try {
    if (_active) {
      //set active and display content
      $elem.classList.add("active");
      $content.classList.remove("hide");
      $content.classList.add("show");

      if (navigator.userAgent.indexOf('Edge') >= 0) {} else {
        AnimateAccordionItem($content);
      }
    } else {
      //reset item and hide content
      $elem.classList.remove("active");
      $content.classList.add("hide");
      $content.classList.remove("show");
    }
  } catch (error) {
    console.error("The accordion item (" + $elem + ") cannot be found.\n\n" + error);
  }
}

;

function AnimateAccordionItem($content) {
  try {
    $content.animate([{
      maxHeight: '0'
    }, {
      maxHeight: '500px'
    }], 400);
    $content.animate([{
      opacity: '0'
    }, {
      opacity: '1'
    }], 1000);
  } catch (error) {
    console.error("The accordion content (" + $content + ") cannot be found.\n\n" + error);
  }
}
/*
      ,'``.._   ,'``.
     :,--._:)\,:,._,.:       All Glory to
     :`--,''   :`...';\      the HYPNOTOAD!
      `,'       `---'  `.
      /                 :
     /                   \
   ,'                     :\.___,-.
  `...,---'``````-..._    |:       \
    (                 )   ;:    )   \  _,-.
     `.              (   //          `'    \
      :               `.//  )      )     , ;
    ,-|`.            _,'/       )    ) ,' ,'
   (  :`.`-..____..=:.-':     .     _,' ,'
    `,'\ ``--....-)='    `._,  \  ,') _ '``._
 _.-/ _ `.       (_)      /     )' ; / \ \`-.'
`--(   `-:`.     `' ___..'  _,-'   |/   `.)
    `-. `.`.``-----``--,  .'
      |/`.\`'        ,',');
          `         (/  (/
 */