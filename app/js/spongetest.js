/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

=====================================================
 SPONGE UK DEVELOPER TEST
 Page-specific JS
=====================================================
*/


/**
 * A new instance of the content parser using the content JSON file
 */
let newContent = new Content( 'app/data/content.json' );
let newAccordion = new Accordion();

newContent.init( SetContent );

function SetContent()
{
	newContent.populateContent( 'header-template', 'header', 'header' );
	newContent.populateContent( 'task-template', 'tasks', 'tasks' );
	newContent.populateContent( 'content-template', 'content', 'content' );
	newContent.populateContent( 'documentation-template', 'docs', 'documentation' );
	newContent.populateContent( 'aboutme-template', 'aboutme' , 'aboutme' );
	newAccordion.populateAccordion( 'aboutme-accordion' );
}


/**
 * Register a Handlebars helper for the difficulty stars
 */
Handlebars.registerHelper( 'difficulty',
	function( intStars ) {
		var strHTMLStarsOut = '';

		for( var intStar = 0; intStar < intStars; intStar++ ) {
			strHTMLStarsOut += '<i class="fa fa-star"></i>';
		}

		for( var intBlankStar = intStars; intBlankStar < 5; intBlankStar++ ) {
			strHTMLStarsOut += '<i class="fa fa-star-o"></i>';
		}

		return strHTMLStarsOut;
	}
);

/**
 * When the content file is ready, actually populate the content
 */
			

