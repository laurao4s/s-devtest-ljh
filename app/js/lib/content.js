/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

 =====================================================
 SPONGE UK DEVELOPER TEST
 JSON parser and event handler
 =====================================================
*/

let objContent = {};

class Content 
{
	
	constructor( strDataLocation ) {
		this.strDataLocation = strDataLocation;
	}
	
	init ( _func ){
		$.getJSON( this.strDataLocation,
			function( objResponse ) {
				objContent = objResponse;
				
				//callback once json loaded
				_func();
			}
		);
		
	};

	/**
	 * Get an item from the content data
	 */
	getItem( intItem ) {
		return objContent[intItem];
	};

	/**
	 * Populate the content
	 */
	populateContent( $source, _item, $elem ) {
		
		try {
			const source = document.getElementById( $source );
			
			let strHeaderSource = source.innerHTML,
				resHeaderTemplate = Handlebars.compile( strHeaderSource ),
				strHeaderHTML = resHeaderTemplate( this.getItem( _item ) );

			const elem = document.getElementById( $elem );
			elem.insertAdjacentHTML('beforeend', strHeaderHTML);

			
		} catch ( error ) {
			console.error( "The following error has occured populating the content (" + $source + "):\n\n" + error );
		}
		
	};
	
}

class Accordion 
{
	
	constructor() {
		
	}
	
	/**
	 * Populate the accordion within the element ($elem) param
	 */
	populateAccordion( $elem ) {
						
		try {
			
			const el = document.getElementById( $elem );
			let items = el.querySelectorAll( ".accordion-item" );
			let contents = el.querySelectorAll( ".accordion-item-content" );
			
			//loop through each accordion item
			for (let i = 0; i < items.length; i++) 
			{
				//set first accordion item as active
				if(i == 0)
					ActiveAccordionItem( items[i], contents[i], true );
				else
					//and hide the rest of the items content
					ActiveAccordionItem( items[i], contents[i], false );
				
			
				//set click interactivity
				items[i].addEventListener("click", function( ) {
					
					//reset all accordion items and hide all content
					for (let j = 0; j < items.length; j++) 
					{
						ActiveAccordionItem( items[j], contents[j], false );
					}
			
					//set selected as active and display the corresponding content
					ActiveAccordionItem( items[i], contents[i], true );
				});
			 }

		} catch ( error ) {
			console.error( "The accordion (" + $elem + ") cannot be found. \n\n" + error );
		}
	};

	
}

//set the active state of an accordion item and it's corrsponding content
function ActiveAccordionItem ( $elem, $content, _active ) {
	try {
		
		if(_active)
		{
			//set active and display content
			$elem.classList.add("active");
			$content.classList.remove("hide");
			$content.classList.add("show");
			if (navigator.userAgent.indexOf('Edge') >= 0){ }else{ AnimateAccordionItem( $content ); }
		}else{
			//reset item and hide content
			$elem.classList.remove("active");
			$content.classList.add("hide");
			$content.classList.remove("show");
		}
		
	} catch ( error ) {
		console.error( "The accordion item (" + $elem + ") cannot be found.\n\n"  + error);
	}
};

function AnimateAccordionItem( $content ) {
	try {

		$content.animate([
		  { maxHeight: '0' },
		  { maxHeight: '500px' }
		], 400);
		
		$content.animate([
		  { opacity: '0' },
		  { opacity: '1' }
		], 1000);
		
	} catch ( error ) {
		console.error( "The accordion content (" + $content + ") cannot be found.\n\n"  + error);
	}
}



/*
      ,'``.._   ,'``.
     :,--._:)\,:,._,.:       All Glory to
     :`--,''   :`...';\      the HYPNOTOAD!
      `,'       `---'  `.
      /                 :
     /                   \
   ,'                     :\.___,-.
  `...,---'``````-..._    |:       \
    (                 )   ;:    )   \  _,-.
     `.              (   //          `'    \
      :               `.//  )      )     , ;
    ,-|`.            _,'/       )    ) ,' ,'
   (  :`.`-..____..=:.-':     .     _,' ,'
    `,'\ ``--....-)='    `._,  \  ,') _ '``._
 _.-/ _ `.       (_)      /     )' ; / \ \`-.'
`--(   `-:`.     `' ___..'  _,-'   |/   `.)
    `-. `.`.``-----``--,  .'
      |/`.\`'        ,',');
          `         (/  (/
 */
